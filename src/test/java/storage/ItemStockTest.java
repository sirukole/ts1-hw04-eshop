package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import shop.StandardItem;

public class ItemStockTest {
    Item refItemT;
    ItemStock itemStockTest;

    @BeforeEach
    public void setUp() {
        refItemT = new StandardItem(1, "Nazev", 50.5F, "Tech", 1);

        itemStockTest = new ItemStock(refItemT);
    }

    @Test
    public void constructorTest() {

        Assertions.assertEquals(refItemT, itemStockTest.getItem());
        Assertions.assertEquals(0, itemStockTest.getCount());
    }

    @ParameterizedTest(name = "First increase: {0}, second increase: {1}, total expected: {2}")
    @CsvSource({"2,3,5", "4,7,11", "3,0,3"})
    public void increaseItemCountTest(int one, int two, int expected) {

        itemStockTest.IncreaseItemCount(one);
        itemStockTest.IncreaseItemCount(two);

        Assertions.assertEquals(expected, itemStockTest.getCount());
    }

    @ParameterizedTest(name = "First decrease: {0}, second decrease: {1}, total expected: {2}")
    @CsvSource({"2,3,-5", "4,7,-11", "3,0,-3"})
    public void decreaseItemCountTest(int one, int two, int expected) {

        itemStockTest.decreaseItemCount(one);
        itemStockTest.decreaseItemCount(two);

        Assertions.assertEquals(expected, itemStockTest.getCount());
    }
}
