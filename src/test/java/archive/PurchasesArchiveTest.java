package archive;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

public class PurchasesArchiveTest {
    Item one;
    Item two;
    Item three;
    PurchasesArchive archiveT;
    ArrayList<Order> orders;

    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    //takovy buffer, do ktereho se uklada co do steamu prileti

    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(out));
        one = new StandardItem(1, "Nazev1", 50.5F, "Tech", 1);
        two = new StandardItem(2, "Nazev2", 60.5F, "Non-Tech", 1);
        three = new StandardItem(3, "Nazev3", 5350.5F, "Gym", 1);
    }
    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public  void printItemTest() {

        ItemPurchaseArchiveEntry itemPurchaseT1 = new ItemPurchaseArchiveEntry(one);
        ItemPurchaseArchiveEntry itemPurchaseT2 = new ItemPurchaseArchiveEntry(two);
        ItemPurchaseArchiveEntry itemPurchaseT3 = new ItemPurchaseArchiveEntry(three);

        HashMap<Integer, ItemPurchaseArchiveEntry> newT = new HashMap<>();

        newT.put(1, itemPurchaseT1);
        newT.put(2, itemPurchaseT2);
        newT.put(3, itemPurchaseT3);

        archiveT = new PurchasesArchive(newT, null);

        String phrase = "ITEM PURCHASE STATISTICS:";

        archiveT.printItemPurchaseStatistics();

        Assertions.assertEquals(phrase+System.lineSeparator()+itemPurchaseT1.toString()+
                System.lineSeparator()+itemPurchaseT2.toString()
                +System.lineSeparator()+itemPurchaseT3.toString()+System.lineSeparator(), out.toString());
    }

    @Test
    public void getHowManyTimesHasBeenItemSoldTest() {

        orders = new ArrayList<>();
        HashMap<Integer, ItemPurchaseArchiveEntry> newT = new HashMap<>();

        ItemPurchaseArchiveEntry itemEntry1 = Mockito.mock(ItemPurchaseArchiveEntry.class);
        ItemPurchaseArchiveEntry itemEntry2 = Mockito.mock(ItemPurchaseArchiveEntry.class);
        ItemPurchaseArchiveEntry itemEntry3 = Mockito.mock(ItemPurchaseArchiveEntry.class);

        newT.put(1, itemEntry1);
        newT.put(2, itemEntry2);
        newT.put(3, itemEntry3);

        archiveT = new PurchasesArchive(newT, orders);

        Mockito.doReturn(5).when(itemEntry1).getCountHowManyTimesHasBeenSold();
        Mockito.doReturn(6).when(itemEntry2).getCountHowManyTimesHasBeenSold();
        Mockito.doReturn(1).when(itemEntry3).getCountHowManyTimesHasBeenSold();

        int result1 = archiveT.getHowManyTimesHasBeenItemSold(one);
        int result2 = archiveT.getHowManyTimesHasBeenItemSold(two);
        int result3 = archiveT.getHowManyTimesHasBeenItemSold(three);

        Assertions.assertEquals(5, result1);
        Assertions.assertEquals(6, result2);
        Assertions.assertEquals(1, result3);
    }

    @Test
    public void putOrderToPurchasesArchiveTest() {
        ShoppingCart card = new ShoppingCart();
        card.addItem(one);
        card.addItem(two);
        Order order = new Order(card, "Customer1", "street", 1);

        archiveT = new PurchasesArchive();
        int oneI = archiveT.getHowManyTimesHasBeenItemSold(one);
        int twoI = archiveT.getHowManyTimesHasBeenItemSold(two);

        archiveT.putOrderToPurchasesArchive(order);

        int oneY = archiveT.getHowManyTimesHasBeenItemSold(one) - 1;
        int twoY = archiveT.getHowManyTimesHasBeenItemSold(two) - 1;

        Assertions.assertEquals(oneI, oneY);
        Assertions.assertEquals(twoI, twoY);

    }


}
