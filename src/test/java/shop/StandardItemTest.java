package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import java.util.stream.Stream;

public class StandardItemTest {
    int idT = 1;
    String nameT = "Item Name";
    float priceT = (float) 100.56;
    String categoryT = "Category Name";
    int loyaltyPointT = 10;

    @Test
    public void constructorTest() {

        StandardItem itemResult = new StandardItem(idT, nameT, priceT, categoryT, loyaltyPointT);

        Assertions.assertEquals(idT, itemResult.getID());
        Assertions.assertEquals(nameT, itemResult.getName());
        Assertions.assertEquals(priceT, itemResult.getPrice());
        Assertions.assertEquals(categoryT, itemResult.getCategory());
        Assertions.assertEquals(loyaltyPointT, itemResult.getLoyaltyPoints());
    }

    @Test
    public void copyTest() {
        StandardItem itemExpected = new StandardItem(idT, nameT, priceT, categoryT, loyaltyPointT);

        StandardItem itemResult = itemExpected.copy();

        Assertions.assertEquals(itemExpected, itemResult);
    }

    private static Stream<Arguments> dataForMethod() {
        Stream<Arguments> data = Stream.of(
                Arguments.of(new StandardItem(1, "Nazev", 50.5F, "Tech", 1),
                        new StandardItem(1, "Nazev", 50.5F, "Tech", 1), true),
                Arguments.of(new StandardItem(1, "Nazev", 50.5F, "Tech", 1),
                        new StandardItem(2, "Nazev", 50.5F, "Tech", 1), false)
        );
        return data;
    }

    @ParameterizedTest(name = "First obj {0} second obj {1} result {2}")
    @MethodSource("dataForMethod")
    public void mustReturnTrueIfObjectIsItem_or_FalseIfIsNot(Object objectIsItem, Object objectCheck, boolean expected) {
        boolean result = objectIsItem.equals(objectCheck);

        Assertions.assertEquals(expected, result);
    }
}
