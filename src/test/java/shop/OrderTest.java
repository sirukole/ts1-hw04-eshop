package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class OrderTest {

    @Test
    public void constructorTest1() {
        ShoppingCart cartT = new ShoppingCart();
        String nameT = "Customer1";
        String adressT = "street";
        int state = 1;

        Order orderT = new Order(cartT, nameT, adressT, state);

        Assertions.assertEquals(cartT.getCartItems(), orderT.getItems());
        Assertions.assertEquals(nameT, orderT.getCustomerName());
        Assertions.assertEquals(adressT, orderT.getCustomerAddress());
        Assertions.assertEquals(state, orderT.getState());

    }

    @Test
    public void constructorTest2_withoutState() {
        ShoppingCart cartT = new ShoppingCart();
        String nameT = "Customer1";
        String adressT = "street";
        int state = 0;

        Order orderT = new Order(cartT, nameT, adressT);

        Assertions.assertEquals(cartT.getCartItems(), orderT.getItems());
        Assertions.assertEquals(nameT, orderT.getCustomerName());
        Assertions.assertEquals(adressT, orderT.getCustomerAddress());
        Assertions.assertEquals(state, orderT.getState());

    }
}
